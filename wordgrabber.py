# 
# a little python script for grab Oxford 5900+ words (both of US and UK accent sound).
# For the legal limited reasons, I can't  upload all .mp3 accent files, 
# but you can use this script to download from website directly.
# I hope you will enjoy it and going to improve English skills with me together. :)
#
# Thank you
#
# By TK VR  22/6/7
#
#
# ps.
# 1 dont forget 'EdgeDriver' (msedgedriver.exe) save it in the same path of script
# 2 if you want to download mp3 files, please select 'True' or 'False' in config variable
#

# config download UK or US accent ('True' or 'False')
DOWNLOAD_UK = True
DOWNLOAD_US = True


from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


from selenium import webdriver
from selenium.webdriver.common.by import By

from time import sleep
import os
import requests


def download(url:str, target:str):
    txt_header = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
    r = requests.get(url, allow_redirects=True, headers={ "User-Agent" : txt_header })
    open(target, 'wb').write(r.content)

# ============================================================================================

ROOT_URL = r'https://www.oxfordlearnersdictionaries.com'
URL = ROOT_URL + r'/wordlists/oxford3000-5000'

#FIREFOX_PATH = os.path.abspath(os.getcwd()) + r'\geckodriver.exe'
#CHROME_PATH = os.path.abspath(os.getcwd()) + r'\chromedriver.exe'
EDGE_PATH = os.path.abspath(os.getcwd()) + r'\msedgedriver.exe'
#services = Service(EDGE_PATH)

driver = webdriver.Edge(executable_path = EDGE_PATH)
#options = webdriver.FirefoxOptions()
#driver = webdriver.Firefox(service=services, options=options)
#driver = webdriver.Chrome(executable_path = CHROME_PATH)
driver.implicitly_wait(18)
driver.get( URL )

#WebDriverWait(driver, 18, poll_frequency=5).until(EC.alert_is_present(), 'Timed out waiting for simple alert to appear')


XPATH_WORDLIST = '//*[@id="wordlistsContentPanel"]/ul'
all_words = driver.find_element(By.XPATH, XPATH_WORDLIST)
lists = all_words.find_elements(By.TAG_NAME, 'li')

# dev > span(type) > div(uk) > div(us)
fileobj = open("words.txt", "a")
count = 0
for item in lists:
    Word        = item.get_attribute('data-hw')
    Type        = item.find_element(By.XPATH, './/span[@class="pos"]').get_attribute('innerText')
    Level       = item.find_elements(By.XPATH, ".//span[@class='belong-to']")
    Level       = Level[0].get_attribute('innerText') if len(Level) > 0 else ""
    count += 1
    print("no.%d\nword : %s\nType : %s  Level : %s" % (count,Word,Type,Level) )
    #
    if DOWNLOAD_UK or DOWNLOAD_US:
        SOUND_UK    = ""
        SOUND_US    = ""
        tmp_list    = item.find_elements(By.XPATH, ".//div")
        if(len(tmp_list) > 1):
            SOUND_UK    = ROOT_URL + item.find_elements(By.XPATH, ".//div")[1].get_attribute('data-src-mp3')
            SOUND_US    = ROOT_URL + item.find_elements(By.XPATH, ".//div")[2].get_attribute('data-src-mp3')    
        #
        # save to folder
        download(SOUND_UK, "./SOUND_UK/" + Word + "_uk.mp3") if DOWNLOAD_UK and len(SOUND_UK) > 0 else ""
        download(SOUND_US, "./SOUND_US/" + Word + "_us.mp3") if DOWNLOAD_US and len(SOUND_US) > 0 else ""
        print( "UK : %s\nUS : %s" % (SOUND_UK, SOUND_US) )
    #
    # write to files
    fileobj.write( "%d, %s, %s, %s\n" % (count,Word,Type,Level) )
#
fileobj.close()
print("...end")

